//
//  Wage.swift
//  window-shopper
//
//  Created by RM on 05.03.2021.
//

import Foundation

class Wage {
    class func getHours(forWage wage: Double, andPrice price: Double) -> Int {
        return Int(ceil(price / wage))
    }
    class func getHoursString(numOfHours: UInt) -> String {
        if (numOfHours % 10 == 1 && numOfHours != 11) {
            return "час";
        };
        if (numOfHours % 10 < 5 && numOfHours % 10 != 0) {
            return "часа";
        }
        return "часов";
    }
}
